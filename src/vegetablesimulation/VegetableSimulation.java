/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package vegetablesimulation;

/**
 *
 * @author DHAIRYA
 */
public class VegetableSimulation {

    public static void main(String[] args) {
        
        String[] vegetables = {"beet","carrot",};
        
        
    }

    @Override
    public String toString() {
        return "VegetableSimulation{" + '}';
    }

    public VegetableSimulation() {
        
    }
    
    private String colour;
    private String size;
    private String ripe;

    public String getRipe() {
        return ripe;
    }

    public void setRipe(String ripe) {
        this.ripe = ripe;
    }

    public VegetableSimulation(String ripe) {
        this.ripe = ripe;
    }

    public VegetableSimulation(String colour, String size) {
        this.colour = colour;
        this.size = size;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
    
}
